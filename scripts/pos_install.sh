#!/bin/bash 

echo "-----------------------------------------------------------------------------"
echo "----------------------------<<< POS Install script >>>-----------------------"
echo "-----------------------------------------------------------------------------"

set -e -x 

BOX_NAME=$1
BOX_ETH1=$2


# Configure file /etc/hosts in order to fix issues with VitualBox networking 
sed "s/127\.0\.0\.1.*k8s.*/$BOX_ETH1   $BOX_NAME/" -i /etc/hosts

# Install required software to run a kubernetes cluster
apt-get update && apt-get install -y apt-transport-https

# Install docker.io 
curl -fsSL https://get.docker.com | bash 



# Install Kubernetes 

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet kubeadm kubectl

# Create file tips k8s

cat <<EOF > /root/commad_k8s.txt

# Run Cluster 

1 - Init Cluste - Node Master

# kubeadm init --apiserver-advertise-address $(hostname -i) --pod-network-cidr 10.1.0.0/16

#   sudo mkdir $HOME/.kube/
#   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
#   sudo chown $(id -u):$(id -g) $HOME/.kube/config

2 - Join Nodes result command kubeadmin 

 #   kubeadm join --token .....

3 - Configure Pod Network

#    kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

3 -  Configure auto complite

#    echo "source < $(kubectl completion bash)" >> ~/.bashrc 

EOF 